# https://chocolatey.org/packages/dotnetcore-sdk/2.2.401
chocolatey_package 'Install dotnet Core' do
  package_name 'dotnetcore-sdk'
  version node[:languages][:dotnetcore_version]
  action :install
end

# https://chocolatey.org/packages/ruby
chocolatey_package 'Install ruby' do
  package_name 'ruby'
  version node[:languages][:ruby_version]
  action :install
end

# https://chocolatey.org/packages/golang
chocolatey_package 'Install Go' do
  package_name 'golang'
  version node[:languages][:go_version]
  action :install
end

# https://chocolatey.org/packages/nodejs
chocolatey_package 'Install nodejs' do
  package_name 'nodejs'
  version node[:languages][:nodejs_version]
  action :install
end

# https://chocolatey.org/packages/openjdk
chocolatey_package 'Install openjdk' do
  package_name 'openjdk'
  version node[:languages][:openjdk_version]
  action :install
end

